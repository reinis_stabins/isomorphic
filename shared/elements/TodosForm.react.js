import React from 'react';

export default class TodosForm extends React.Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.createTodo(this.refs['todo-input'].value);
    this.refs['todo-input'].value = "";
  }
  render() {
    return (
      <form id="todo-formlist" onSubmit={this.handleSubmit}>
        <input type="text" ref="todo-input"/>
        <input type="submit" value="submit" onClick={this.handleSubmit} />
      </form>
    );
  }
}
