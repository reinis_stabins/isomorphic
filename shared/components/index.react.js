import React from 'react';

export default class App extends React.Component {
  render() {
    const {children} = this.props
    return (
      <div id="app-view">
        <h1>Todos</h1>
        <hr />
        {children}
      </div>
    );
  }
}
