import React                  from 'react';
import TodosView              from '../../elements/TodosView.react';
import TodosForm              from '../../elements/TodosForm.react';
import { bindActionCreators } from 'redux';
import * as TodoActions       from '../../actions/todoActions';
import { connect }            from 'react-redux';

@connect(state => ({ state: state }))
export default class Home extends React.Component {
   render() {
     const { state, dispatch } = this.props;
    return (
      <div>
          <h2>Home</h2>
            <TodosView todos={state.todoReducer}
            {...bindActionCreators(TodoActions, dispatch)} />
            <TodosForm
            {...bindActionCreators(TodoActions, dispatch)} />
      </div>
    )
   }
}
