import todoReducer from "./todoReducer";
import { routerReducer } from "react-router-redux";

export default {
  todoReducer,
  routing:routerReducer
};
