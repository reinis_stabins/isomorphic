import {fromJS} from 'immutable';
import {CREATE_TODO, EDIT_TODO, DELETE_TODO} from '../actions/todoActions';

const initialState = new fromJS(["dssd"]);

export default function todoReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_TODO:
      return state.push(action.text);
      break;
    case EDIT_TODO:
      return state.set(action.id, action.text);
      break;
    case DELETE_TODO:
      return state.delete(action.id);
      break;
    default:
      return state;
  }
}
