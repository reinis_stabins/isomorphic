import express from 'express';
import path from "path"
import React                     from 'react';
import { renderToString, renderToStaticMarkup } from 'react-dom/server'
import { RouterContext, match } from 'react-router';
import {createMemoryHistory, browserHistory} from 'react-router';
import routes                    from './shared/routes.js';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { Provider }                     from 'react-redux';
import reducers                    from './shared/reducers';
import { routerMiddleware } from "react-router-redux";
import {fromJS} from "immutable";

const app = express();

if (process.env.NODE_ENV !== 'production') {
  require('./webpack.dev').default(app);
}
app.use(express.static(path.join(__dirname, 'dist')));

app.use((req, res) => {
  const reducer = combineReducers(reducers);
  const middleware = routerMiddleware(browserHistory);
  const store   = createStore(reducer, applyMiddleware(middleware));
  const location = createMemoryHistory().createLocation(req.url);

  match({ routes, location }, (err, redirectLocation, renderProps) => {
    if (err) {
      console.error(err);
      return res.status(500).end('Internal server error');
    }
    if (!renderProps) return res.status(404).end('Not found.');

    const InitialView = (
      <Provider store={store}>
        <RouterContext {...renderProps} />
      </Provider>
    );
    const componentHTML = renderToString(InitialView);
    const HTML = '<!DOCTYPE html>' + renderToStaticMarkup(
      <html>
        <head>
          <meta charSet="utf-8" />
          <title>Universal App</title>
          <script dangerouslySetInnerHTML={{__html: `
              window.__INITIAL_STATE__ = ${JSON.stringify(store.getState())};
          `}}></script>
        </head>
        <body>
          <div>
            <div id="react-app" dangerouslySetInnerHTML={{__html:componentHTML}}>
            </div>
          </div>
          <script type="application/javascript" src="/bundle.js"></script>
        </body>
      </html>
    );
    res.end(HTML);
  });
});
export default app;
