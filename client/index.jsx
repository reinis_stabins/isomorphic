import React       from 'react';
import { render }  from 'react-dom';
import { Router, browserHistory }  from 'react-router';
import {combineReducers, createStore, applyMiddleware} from 'redux';
import { Provider }         from 'react-redux';
import {syncHistoryWithStore, routerMiddleware} from 'react-router-redux';
import routes      from '../shared/routes.js';
import reducers                  from '../shared/reducers';
import { fromJS }                       from 'immutable';

var initialState = window.__INITIAL_STATE__;
Object
  .keys(initialState)
  .forEach(key => {
    initialState[key] = fromJS(initialState[key]);
   });
const reducer = combineReducers(reducers);
// const middleware = routerMiddleware(browserHistory);
const store   = createStore(reducer, initialState);
const history = syncHistoryWithStore(browserHistory, store);

render(
  <Provider store={store}>
      <Router children={routes} history={history} />
  </Provider>,
  document.getElementById('react-app')
);
